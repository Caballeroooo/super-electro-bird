﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningsLeft : MonoBehaviour
{
    public float speed;
    private float deadX = 20f;
    
    void Update()
    {
        if (transform.position.x > deadX)
        {
            Destroy(gameObject);
        }

        transform.Translate(Vector3.right * speed * Time.deltaTime);
    }
}
