﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerLightningsRight : MonoBehaviour
{

    public LightningsRight lightningRight;
    public float spawnTime;
    public GameObject firstLightning;
    private float timerLightning;
    private bool isInit;
    private float randDigit;
    

    public void SpawnLightning()
    {
        Instantiate(lightningRight, transform.position, Quaternion.identity);
        randDigit = Random.Range(0, 3f);
    }
    
    void Update()
    {
        if (!isInit)
            return;
        
        firstLightning.SetActive(true);
        
        if (firstLightning.transform.position.x <= -15)
            firstLightning.SetActive(false);
        
        timerLightning += Time.deltaTime;
        if (timerLightning >= spawnTime + randDigit)
        {
            timerLightning = 0;
            SpawnLightning();
        }
        Debug.Log(randDigit);
    }
    
    public void Init(bool isActive)
    {
        isInit = isActive;
    }
}
