﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;
    public SliderAnim powerSlider;
    public CounterScope score;

    public GameObject Menu;
    public GameObject Game;
    public GameObject EndGame;
    public GameObject Audio;
    public GameObject AudioDeath;

    public SpawnerLightningsRight gameSpawner;
    public SpawnerLightningsLeft gameSpawner2;
    public SpawnerLightningsRight gameSpawner3;
    public SpawnerLightningsLeft gameSpawner4;
    public SpawnerLightningsRight gameSpawner5;

    public Text MenuBestScoreValue;
    public Text EndGameBestScoreValue;
    public Text GameScoreValue;
    public Text EndGameScoreValue;
    
    private int fastStartMode;

    void Awake()
    {
        instance = this;
    }


    void Update()
    {
        GameScoreValue.text = ((int) score.score).ToString();
    }

    public void ViewPower(BirdPower value)
    {
        powerSlider.ViewPower(value);
    }

    public void OnReset() //Кнопка в игре
    {
        SceneManager.LoadScene("Game");
        PlayerPrefs.SetInt("fastMode", 0);
    }

    public void EndGameState()
    {
        Menu.SetActive(false);
        Game.SetActive(false);
        EndGame.SetActive(true);
        Audio.SetActive(false);
        AudioDeath.SetActive(true);

        gameSpawner.Init(false);
        score.Init(false);
        score.NewBestResult();
        EndGameScoreValue.text = ((int) score.score).ToString();
        EndGameBestScoreValue.text = ((int) score.GetBestScore()).ToString();
    }
    
    public void OnPlayAgain() //Кнопка в конце игры
    {
        SceneManager.LoadScene("Game");
        PlayerPrefs.SetInt("fastMode", 1);
    }

    private void StartGame()
    {
        Menu.SetActive(false);
        Game.SetActive(true);
        EndGame.SetActive(false);
        Audio.SetActive(true);
        AudioDeath.SetActive(false);

        gameSpawner.Init(true);
        gameSpawner2.Init(true);
        gameSpawner3.Init(true);
        gameSpawner4.Init(true);
        gameSpawner5.Init(true);
        score.Init(true);
    }

    void Start()
    {
        Menu.SetActive(true);
        Game.SetActive(false);
        EndGame.SetActive(false);
        Audio.SetActive(true);
        AudioDeath.SetActive(false);

        gameSpawner.Init(false);
        gameSpawner2.Init(false);
        gameSpawner3.Init(false);
        gameSpawner4.Init(false);
        gameSpawner5.Init(false);

        fastStartMode = PlayerPrefs.GetInt("fastMode", 0);

        if (fastStartMode == 1)
        {
            StartGame();
        }
        
        MenuBestScoreValue.text = ((int) score.GetBestScore()).ToString();
    }

    private void OnPlayClick() //Кнопка в начальном меню
    {
        StartGame();
    } 
    
    private void OnApplicationQuit()
    {
        PlayerPrefs.SetInt("fastMode", 0);
    }

    public void OnQuit()
    {
        Application.Quit();
    }
    
    
} 