﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillEvent : MonoBehaviour
{
    [SerializeField] private SliderAnim owner;
    
    public void EndAnimationFill()
    {
        owner.EndAnim();
    }
}
