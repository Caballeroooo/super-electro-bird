﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour
{
    public Rigidbody2D rigidbody;
    public float force;
    public Animator anim;
    public CameraController camControll;
    private BirdPower power = new BirdPower(100);

    private float timer;
    
    void Update()
    {
        if (power.GetCurrentPower() != 0)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                rigidbody.AddForce(Vector2.up * force, ForceMode2D.Impulse);
                power.AddPower(-10);
                anim.SetBool("Flying", true);
            }
        }

        UIManager.instance.ViewPower(power);
        
    }
    
    public void playerDeath()
    {
        anim.SetBool("Flying", false);
        anim.SetTrigger("Death");
        camControll.StopFollow(true);
        UIManager.instance.EndGameState();
    }
    
    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.layer == LayerMask.NameToLayer("Lightning"))
            playerDeath();

        if (col.gameObject.layer == LayerMask.NameToLayer("Cabel"))
        {
            anim.SetBool("Flying", false);
            power.AddPower(100);
        }
    }
    
}
