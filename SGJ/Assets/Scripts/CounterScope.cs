﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CounterScope : MonoBehaviour

{
    public float score = 0;
    public float bestScore;
    private const string keyBestScore = "BestScore";
    private bool isInit;
    
    void Start()
    {
        GetBestScore();
    }

    void Update()
    {
        if (!isInit)
            return;
        
        score += 5 * Time.deltaTime;
    }
    
    public float GetBestScore()
    {
        bestScore = PlayerPrefs.GetFloat(keyBestScore, 21);
        return bestScore;
    }

    public void Init(bool isActive)
    {
        isInit = isActive;
    }

    public void NewBestResult()
    {
        if (score > bestScore)
            PlayerPrefs.SetFloat(keyBestScore, score);
    }
}
