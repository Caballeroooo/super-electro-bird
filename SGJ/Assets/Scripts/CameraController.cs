﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject bird;
    private Vector3 offset;
    private bool isFollow = true;



    void Start()
    {
        offset = transform.position - bird.transform.position;
    }


    void LateUpdate()
    {
        if (!isFollow)
            return;
        transform.position = bird.transform.position + offset;
    }

    public void StopFollow(bool isStop)
    {
        isFollow = !isStop;
    }
}
