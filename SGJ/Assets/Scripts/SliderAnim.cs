﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderAnim : MonoBehaviour
{
    public Slider mainSlider;
    public Slider blinkerSlider;
    public Animator anim;
    private float lastMaxPower = float.MinValue;
    private float deltaHP;

    private void Start()
    {
        mainSlider.minValue = 0;
        blinkerSlider.minValue = 0;
    }

    public void ViewPower(BirdPower player)
    {
        if (lastMaxPower == float.MinValue)
        {
            lastMaxPower = player.GetMaxPower();
            blinkerSlider.maxValue = player.GetMaxPower();
            blinkerSlider.value = player.GetMaxPower();
        }
        
        deltaHP = lastMaxPower - player.GetCurrentPower();
        mainSlider.maxValue = player.GetMaxPower();
        lastMaxPower = mainSlider.value = player.GetCurrentPower();
        
        if (Math.Abs(deltaHP) > 0)
            Blinker();
    }

    public void EndAnim()
    {
        blinkerSlider.value = mainSlider.value;
    }

    private void Blinker()
    {
        anim.SetTrigger("Blink");
    }
}
