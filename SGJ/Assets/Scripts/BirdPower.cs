﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdPower
{
    private float power;
    private float maxPower;

    public BirdPower(float maxValue)  
    {
        maxPower = maxValue;
        power = maxValue;
    }
    
    public float GetCurrentPower()
    {
        return power;
    }

    public float GetMaxPower()
    {
        return maxPower;
    }
    
    public void SetMaxPower()
    {
        power = maxPower;
    }

    public void AddPower(float value)
    {
        power += value;
        if (power <= 0)
            power = 0;
        if (power >= maxPower)
            power = maxPower;
    }

}
