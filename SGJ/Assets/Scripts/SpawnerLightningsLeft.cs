﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerLightningsLeft : MonoBehaviour
{

    public LightningsLeft lightningLeft;
    public float spawnTime;
    private float timerLightning;
    private bool isInit;
    private float randDigit;


    public void SpawnLightning()
    {
        Instantiate(lightningLeft, transform.position, Quaternion.identity);
        randDigit = Random.Range(0, 4f);
    }
    
    void Update()
    {
        if (!isInit)
            return;
        
        timerLightning += Time.deltaTime;
        if (timerLightning >= spawnTime + randDigit)
        {
            timerLightning = 0;
            SpawnLightning();
        }
        
    }
    
    public void Init(bool isActive)
    {
        isInit = isActive;
    }
}
