﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningsRight : MonoBehaviour
{
    public float speed;
    private float deadX = -20f;
    
    void Update()
    {
        if (transform.position.x < deadX)
        {
            Destroy(gameObject);
        }

        transform.Translate(Vector3.left * speed * Time.deltaTime);
    }
}
