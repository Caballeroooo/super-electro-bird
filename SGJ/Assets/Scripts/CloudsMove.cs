﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudsMove : MonoBehaviour
{
    public float speed;
    public GameObject cloudPack1;
    public GameObject cloudPack2;
    private float transferX = 42;
    private float stopPosX = 22;
    
    

    void Update()
    {
        transform.Translate(Vector3.right * speed * Time.deltaTime);
        if (cloudPack1.transform.position.x > stopPosX)
        {
            cloudPack1.transform.position = new Vector2(cloudPack1.transform.position.x - transferX, cloudPack1.transform.position.y);
        }
        if (cloudPack2.transform.position.x > stopPosX)
        {
            cloudPack2.transform.position = new Vector2(cloudPack2.transform.position.x - transferX, cloudPack2.transform.position.y);
        }
        
    }
}
